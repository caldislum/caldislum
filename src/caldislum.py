#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2009 Nicolas Gruel All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version. It is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

# This file is part of caldislum

# .. math:: Ceci est un test
#             \lambda\Omega    
    
# 4pi = 41252.96 deg² = 148.5e6 arcmin²
# TODO: add unitest or doctest!!!

from math import log10, sqrt, sin, sinh, pi
from math import asin as arcsin 
from math import asinh as arcsinh

from integration import closedpoints

__docformat__ = "restructuredtext en"

class Flrw:

    """Class to calcul the distance luminosity and some
    transformation between parsec and arcsec for different cosmology.
    It is based on David Hogg paper :
    
    http://xxx.unizar.es/abs/astro-ph/9905116
    
    result checked with with Ned Wright cosmology calculator
    
    http://www.astro.ucla.edu/~wright/CosmoCalc.html
                
    Flrw class constructor:
        
    Parameters
    ----------
    
    H0 : float
         :math:`H0` the hubble constant in km/s/Mpc (default 71 km/s/Mpc)
    OmegaM : float
             :math:`{\Omega}_M` the matter density (dimensionless) (default 0.27)
    OmegaL : float 
             :math:`{\Omega}_{\lambda}` the vacuum energy density (dimensionless) (default 0.73)
    
    Examples
    --------
    >>> import caldislum
    >>> print caldislum.Flrw(OmegaM=0.6).dl(z=0.1)
    450.669713172
    
    >>> import caldislum   
    >>> dislum = caldislum.Flrw(H0=50)
    >>> print dislum.DM(z=0.1)
    39.0507253513
    
    >>> import caldislum
    >>> dislum = caldislum.Flrw(70,0.3,0.7)
    >>> dislum.dl(z=1)
    6607.6575441767363

    """
    
    def __init__(self,H0=71.0,OmegaM=0.27,OmegaL=0.73):        
        self.c_lum  = 299792.458                 # speed light in meter
        self.__H0     = H0                         # in km s^-1 Mpc^-1
        self.__OmegaM = OmegaM
        self.__OmegaL = OmegaL
        self.__OmegaK = 1.0 - OmegaM - OmegaL
        self.__DH = self.c_lum/self.__H0             # in Mpc
        self.__h = self.__H0 / 100.                  # 
        #self.t_h = 1 / H0 
        self.__t_h = 9.778 /self.__h                 # to have it in normal unit

    def get_hubble(self):
        '''Return the Hubble constant value used:
        
        Returns
        -------
        H0 : float
             :math:`H0` is the Hubble constant 
        '''
        return self.__H0
    
    def get_hubble_time(self):
        '''Return the Hubble time value used:
        
        Returns
        -------
        th : float
             :math:`th` is the Hubble time 
        '''
        return self.__t_h
    
    def get_matter(self):
        '''Return the matter density :math:`{\Omega}_M` value used:
        
        Returns
        -------
        OmegaM : float
                 :math:`{\Omega}_M` is the matter density
        '''
        return self.__OmegaM
    
    def get_vacuum(self):
        '''Return the vacuum density :math:`{\Omega}_{\lambda}` value used:
        
        Returns
        -------
        OmegaL : float
                 :math:`{\Omega}_{\lambda}` the vacuum density
        '''
        return self.__OmegaL
    
    def set_hubble(self, H0):
        '''Set the Hubble constant :math:`H0` value and all the values derived directly from it
        (Hubble time t_h ) for the instance of Flrw. 
        
        Parameters
        ----------
        H0 : float
             :math:`H0` is the Hubble constant 

        '''
        self.__H0 = H0
        self.__DH = self.c_lum/self.__H0
        self.__h = self.__H0 / 100.
        self.__t_h = 9.778 /self.__h
    
    def set_matter(self,OmegaM):
        '''Set the matter density :math:`{\Omega}_M` value for the instance of Flrw. 
        
        Parameters
        ----------
        OmegaM : float
                 :math:`{\Omega}_M` is the matter density
        '''
        self.__OmegaM = OmegaM
    
    def set_vacuum(self, OmegaL):
        '''Set the vacuum density :math:`{\Omega}_{\lambda}` value for the instance of Flrw.
        
        Parameters
        ----------
        OmegaL : float
                 :math:`{\Omega}_{\lambda}` the vacuum density
                 
        '''
        self.__OmegaL = OmegaL
    
    def Ez(self,z):
        '''
        This function is the invert of the one's use by Peebles 1993 
        or Hogg 2000 (for numerical convenience).
        This function is used to calcul the line-of-sight comoving distance.
        
        Parameters
        ----------
        z : float
            redshift        
        '''
        return (1. / sqrt( (1+z)**3 * self.__OmegaM +
                           (1+z)**2 * self.__OmegaK +
                           self.__OmegaL))

    def Ez_tl(self,z):
        '''This function is the invert of the one's use by Peebles 1993 
        or Hogg 2000 (for numerical convenience).
        This function is used to calcul the lt.
        It's the same than above but with the (1+z) factor in addition.
        
        Parameters
        ----------
        z : float
            redshift
        '''
        return (1/((1+z) * sqrt((1+z)**3 * self.__OmegaM + 
                                (1+z)**2 * self.__OmegaK + 
                                self.__OmegaL)))

    def dc(self, z):
        """Calcul the line of sight comoving distance in Mpc for a certain redshift z
        and a certain cosmology.
        
        Parameters
        ----------
        z : float
            redshift
        
        Returns
        -------
        dc : float
             line of sight comoving distance (Mpc).
        """ 
        return (self.__DH * closedpoints(self.Ez,0,z))

    def dm(self, z):
        """Calcul the transverse comoving distance in Mpc for a certain redshift z
        and a certain cosmology.
        
        Parameters
        ----------
        z : float
            redshift
            
        Returns
        -------
        dm : float
             transverse comoving distance (Mpc).
            
        """
        self.__OmegaK = 1 - self.__OmegaM - self.__OmegaL
        #Hubble Distance  
        self.__DH = self.c_lum/self.__H0 
        if self.__OmegaK < 0:
            return (self.__DH *
                     (1/sqrt(abs(self.__OmegaK)))* 
                     sin(sqrt(abs(self.__OmegaK))*
                         closedpoints(self.Ez,0,z)))
        elif self.__OmegaK > 0:
            return (self.__DH *
                     (1/sqrt(abs(self.__OmegaK)))* 
                     sinh(sqrt(abs(self.__OmegaK))*
                          closedpoints(self.Ez,0,z)))
        else:
            return (self.__DH * closedpoints(self.Ez,0,z))

    def da(self,z):
        '''Angular diameter distance in Mpc. 
        
        Parameters
        ----------
        z : float
            redshift
            
        Returns
        -------
        da : float
             Angular diameter distance (Mpc)
        '''
        return self.dm(z) / (1+z)

    def dl(self,z):
        '''Calcul the luminosity distance in Mpc for a certain redshift z
        and a certain cosmology
        
        Parameters
        ----------
        z : float
            redshift
            
        Returns:
        --------
        dl : float
             luminosity distance (Mpc)
        '''
        return (1+z) * self.dm(z)

    def vol(self, z):
        '''
        Calcul the comoving volume within the redshfit z (in :math:`Gpc^{3}`)
        
        Parameters
        ----------
        z : float
            redshift
            
        Returns
        -------
        vol : float
              comoving volume (:math:`Gpc^{3}`)
        '''
        
        self.__OmegaK = 1 - self.__OmegaM - self.__OmegaL
        #Hubble Distance          
        self.DMonDH = self.dm(z) / self.__DH
        if self.__OmegaK < 0:
            return ( (4*pi*self.__DH**3 / (2*self.__OmegaK)) *
                     (self.DMonDH * sqrt(1+self.__OmegaK*self.DMonDH**2) -
                      (1/sqrt(abs(self.__OmegaK))) *
                      arcsin(sqrt(abs(self.__OmegaK))*self.DMonDH)))
        elif self.__OmegaK > 0:
            return ( (4*pi*self.__DH**3 / (2*self.__OmegaK)) *
                     ( self.DMonDH * sqrt(1+self.__OmegaK*self.DMonDH**2) -
                       (1/sqrt(abs(self.__OmegaK))) *
                       arcsinh(sqrt(abs(self.__OmegaK))*self.DMonDH)))
        else:
            return  (4*pi*self.dm(z)**3 / 3)

    def DM(self,z):
        """Calcul the distance modulus for a certain redshift and cosmolgy (in mag) 
        
        Parameters
        ----------
        z : float
            redshift
            
        Returns
        -------
        DM : float
             the distance modulus (mag)
        """
        if z==0:
            return 0
        else:
            return 5*log10((self.dl(z)*1e6)/10)

    def lt(self,z):
        """
        Calcul the difference between the age t_0 of the Universe now
        and the age t_e of the Universe at the time the photons were
        emitted.
        
        Parameters
        ----------
        z : float
            redshift
            
        Returns
        -------
        lt : float
             lookback time (Gyrs)
        """
        return float(self.__t_h) * closedpoints(self.Ez_tl,0,z)
    
    def age(self, z=0):
        """Function to return the age of the universe at a certain redshift in Gyr
        
        Parameters
        ----------
        z : float
            redshift
            
        Returns
        -------
        age : float
             lookback time (Gyrs)
        """
        return self.__t_h - self.lt(z) 

    def pc2arcsec(self,z,pc=1):
        
        """Transform from parsec to arcsec.
        
        - :math:`1\\ arcsec = \\frac{1}{60}\\ arcmin`                                
        - :math:`1\\ min\\ d'arc = \\frac{1}{60}\\ deg`
        - :math:`1\\ arcsec = \\frac{1}{60} \\times \\frac{1}{60} = \\frac{1}{3600}\\ deg`
           
        Note: :math:`\\frac{d(deg)}{180} = \\frac{r(rad)}{\\pi}`        
        => :math:`1\\ rad = \\frac{180}{\\pi} \\times 3600 = 206264.8062\\ arcsec`
        
        Parameters
        ----------
        z : float
            redshift
        pc : float
             the physical size you want to transform (pc)
            
        Returns
        -------
        angle : float
                angle in arcsec
        """
        if z==0:
            return pc
        else:
	    return 1 / self.arcsec2pc(z,1) * pc

    def arcsec2pc(self,z,arcsec=1):
        """
        Transform from arcsec to parsec.
        
        - :math:`1\\ arcsec = \\frac{1}{60}\\ arcmin`                                
        - :math:`1\\ min\\ d'arc = \\frac{1}{60}\\ deg`
        - :math:`1\\ arcsec = \\frac{1}{60} \\times \\frac{1}{60} = \\frac{1}{3600}\\ deg`
           
        Note: :math:`\\frac{d(deg)}{180} = \\frac{r(rad)}{\\pi}`
        => :math:`1\\ rad = \\frac{180}{\\pi} \\times 3600 = 206264.8062\\ arcsec`
        
        Parameters
        ----------
        z : float
            redshift
        angle : float
                angle in arcsec you want to transform.
        
            
        Returns
        -------
        pc : float
             the physical size (pc)
        """
        if z==0:
            return arcsec
        else:
            return (self.da(z)*1e6) / 206264.8062 * arcsec

def main():
    '''Function to use CLI caldislum. Needed by setuptools as I understand them...
    '''
    import sys
    dislum = Flrw()
    if (   '--help' in sys.argv 
        or '-help' in sys.argv 
        or '-h' in sys.argv 
        or len(sys.argv)<2):
        print  'caldislum usage: caldislum z (H0=0.71 OM=0.27 OL=0.73)'
        print  'or graphical interface: caldislum_tk.py'
        print  'or graphical interface: caldislum_qt.py'
    elif len(sys.argv) >= 2:
        z = float(sys.argv[1])  # redshift
        # Hubble constant (km/s/Mpc)
        if len(sys.argv) >= 3: 
            dislum.set_hubble(float(sys.argv[2])) 
        # cosmologie: Omega_m
        if len(sys.argv) >= 4: 
            dislum.set_matter(float(sys.argv[3]))
        # cosmologie: Omega_lambda 
        if len(sys.argv) == 5: 
            dislum.set_vacuum(float(sys.argv[4]))

        print 'z = %6.3f, H0 = %5.2f, OM = %5.2f, OL = %5.2f'% (z,dislum.get_hubble(),dislum.get_matter(), dislum.get_vacuum())
        print 'comoving volume within redshift (0 <= z <= %6.3f) = %e Gpc3' % (z,dislum.vol(z))
        print 'Line of sight comoving distance = %e Mpc' % dislum.dc(z)
        print 'Transverse comoving Distance = %e Mpc' % dislum.dm(z)
        print 'Angular diameter distance = %e Mpc' % dislum.da(z)
        print 'Luminosity distance = %e in pc = %e in Mpc' % (dislum.dl(z)*1e6, dislum.dl(z))         
        print 'Distance Modulus = %e in mag' % dislum.DM(z)
        print 'lookbacktime T = %8.5f Gyr.' % dislum.lt(z)
        print 'Hubble time tH = %8.5f Gyr.' % dislum.get_hubble_time()
        print 'Age(z) = %8.5f Gyr.' % dislum.age(z)
        print '1 pc                = %e arcsec' % dislum.pc2arcsec(z)
        print '1 arcsec            = %e pc' % dislum.arcsec2pc(z)
        
if __name__ == '__main__':
    main()

