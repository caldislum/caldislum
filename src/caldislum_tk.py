#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2009 Nicolas Gruel All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version. It is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

import sys
from caldislum import Flrw
dislum = Flrw()
if len(sys.argv) == 1:

    from Tkinter import *
       
    def ok():
        #global z,H0,OmegaM,OmegaL
        #from dislum import dislum,arcs2pc
        z      = float(entr0.get())
        dislum.H0     = float(entr1.get())
        dislum.OmegaM = float(entr2.get())
        dislum.OmegaL = float(entr3.get())
        caldislum1 = dislum.dl(z)
        pc2arcsec1 = dislum.pc2arcsec(z)
        arcsec2pc1 = dislum.arcsec2pc(z)
#        covolume1 =dislum.covolume()
        distance_modulus1 = dislum.DM(z)
        res1=Label(fenetre,text=str(caldislum1*1e6))
        res1.grid(row=4,column=1)
        res2=Label (fenetre,text=str(caldislum1))
        res2.grid(row=5,column=1)
        res3=Label (fenetre,text=str(pc2arcsec1))
        res3.grid(row=6,column=1)
        res4=Label (fenetre,text=str(arcsec2pc1))
        res4.grid(row=7,column=1)
        res5 = Label(fenetre, text=str(distance_modulus1))
        res5.grid(row=8,column=1)
        
        #print 'DL = %e in pc' % caldislum1
        #print '1 pc = %e arcsec' % pc2arcsec1
        #print '1 arcsec = %e pc' % arcsec2pc1
        
    fenetre = Tk()
    txt0_l=Label(fenetre,text="z ")
    txt1_l=Label(fenetre,text="H0")      
    txt2_l=Label(fenetre,text="Omega_m")
    txt3_l=Label(fenetre,text="Omega_l")

    txt0_r=Label(fenetre,text="redshift")
    txt1_r=Label(fenetre,text="Hubble constant (km/s/Mpc)")
    txt2_r=Label(fenetre,text="Omega_m")
    txt3_r=Label(fenetre,text="Omega_Lambda")

    ## txt0_r2=Label(fenetre,text="default :  0.0")
    ## txt1_r2=Label(fenetre,text="default : 71.0")
    ## txt2_r2=Label(fenetre,text="default :  0.3")
    ## txt3_r2=Label(fenetre,text="default :  0.7")

    res1_l = Label(fenetre,text="DL")
    res1_r = Label(fenetre,text="Luminosity distance in pc")
    res2_l = Label(fenetre,text="DL")
    res2_r = Label(fenetre,text="Luminosity distance in Mpc")
    res3_l = Label(fenetre,text="1pc = ")
    res3_r = Label(fenetre,text="arcsec")
    res4_l = Label(fenetre,text="1arcsec = ")
    res4_r = Label(fenetre,text="pc")
    res5_l = Label(fenetre,text="DM = ")
    res5_r = Label(fenetre,text="mag")

    entr0 = Entry(fenetre)
    entr1 = Entry(fenetre)
    entr2 = Entry(fenetre)
    entr3 = Entry(fenetre)

    txt0_l.grid(row=0,sticky=W)
    txt1_l.grid(row=1,sticky=W)
    txt2_l.grid(row=2,sticky=W)
    txt3_l.grid(row=3,sticky=W)
    res1_l.grid (row=4,sticky=W)
    res2_l.grid (row=5,sticky=W)
    res3_l.grid (row=6,sticky=W)
    res4_l.grid (row=7,sticky=W)
    res5_l.grid (row=8,sticky=W)
    txt0_r.grid(row=0,column=2,sticky=W)
    txt1_r.grid(row=1,column=2,sticky=W)
    txt2_r.grid(row=2,column=2,sticky=W)
    txt3_r.grid(row=3,column=2,sticky=W)
    res1_r.grid(row=4,column=2,sticky=W)
    res2_r.grid(row=5,column=2,sticky=W)
    res3_r.grid(row=6,column=2,sticky=W)
    res4_r.grid(row=7,column=2,sticky=W)
    res5_r.grid(row=8,column=2,sticky=W)
    ## txt0_r2.grid(row=0,column=3,sticky=W)
    ## txt1_r2.grid(row=1,column=3,sticky=W)
    ## txt2_r2.grid(row=2,column=3,sticky=W)
    ## txt3_r2.grid(row=3,column=3,sticky=W)

    entr0.grid(row=0,column=1)
    entr1.grid(row=1,column=1)
    entr2.grid(row=2,column=1)
    entr3.grid(row=3,column=1)

    b = Button(fenetre,text='Execute',command=ok)
    b.grid(row=9)

    stop = Button(fenetre,text='Close',command=fenetre.destroy)
    stop.grid(row=9,column=2)
    fenetre.mainloop()
