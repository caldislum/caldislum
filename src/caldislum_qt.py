#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2009 Nicolas Gruel All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version. It is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import ui_caldislum

try:
    import milia
except ImportError:
    import caldislum as milia

MAC = True
try:
    from PyQt4.QtGui import qt_mac_set_native_menubar
except ImportError:
    MAC = False

flrw = milia.Flrw()

class PyFunl(QMainWindow, ui_caldislum.Ui_Metrics):
    
    def __init__(self, parent=None):
        super(PyFunl, self).__init__(parent)

        self.__H0 = flrw.get_hubble()
        self.__OmegaM = flrw.get_matter()
        self.__OmegaL = flrw.get_vacuum()
   
        self.setupUi(self)
        self.matterLabel.setText(QString(u'\u03a9'+'M'))
        self.vacuumLabel.setText(QString(u'\u03a9'+u'\u03bb'))
        self.redshiftLabel.setText(QString('z'))
        

        self.resetForm()
                
        #Result you want to have.
        self.metrics_method = [self.ageCheckBox, self.ltCheckBox, self.dcCheckBox, self.dmCheckBox,
                               self.dlCheckBox, self.DMCheckBox, self.daCheckBox, self.volCheckBox,
                               self.arcsec2pcCheckBox, self.pc2arcsecCheckBox]
        
        self.checkbox = {self.ageCheckBox : ['Age of the universe', flrw.age, 'Gyr'],
                         self.ltCheckBox : ['Look Back Time',flrw.lt, 'Gyr'],
                         self.dcCheckBox : ['Line of sight comoving distance',flrw.dc, 'Mpc'],
                         self.dmCheckBox : ['Transverse comoving distance', flrw.dm, 'Mpc'],
                         self.dlCheckBox : ['Luminosity distance',flrw.dl, 'Mpc'],
                         self.DMCheckBox : ['Distance Modulus',flrw.DM, 'mag'],
                         self.daCheckBox : ['Angular distance',flrw.da, 'Mpc'],
                         self.volCheckBox : ['Comoving volume',flrw.vol, 'Gpc<sup>3</sup>/sr'],
                         self.arcsec2pcCheckBox : ['arcsec to parsec', flrw.arcsec2pc, 'pc'],
                         self.pc2arcsecCheckBox : ['parsec to arcsec', flrw.pc2arcsec, '"'],
                         }

        if not MAC:            
            self.buttonBox.setFocusPolicy(Qt.NoFocus)        

        self.connect(self.hubbleEdit,SIGNAL('returnPressed()'), self.returnHubble)
        self.connect(self.matterEdit,SIGNAL('returnPressed()'), self.returnMatter)
        self.connect(self.vacuumEdit,SIGNAL('returnPressed()'), self.returnVacuum)

        self.redshiftEdit.setFocus()        
        self.connect(self.redshiftEdit,SIGNAL('returnPressed()'), self.returnRedshift)

        self.connect(self.ageCheckBox, SIGNAL('returnPressed()'), self.ltCheckBox.setFocus)

        self.connect(self.buttonBox.button(QDialogButtonBox.Reset), SIGNAL('clicked()'), self.resetForm)        
        self.connect(self.buttonBox.button(QDialogButtonBox.Apply), SIGNAL('clicked()'), self.apply)
        self.connect(self.buttonBox.button(QDialogButtonBox.Close), SIGNAL('clicked()'), SLOT('close()'))
    
        self.connect(self.CheckAllButton,SIGNAL('clicked()'),self.checkall)
        self.connect(self.UncheckAllButton,SIGNAL('clicked()'),self.uncheckall)

        #status bar
        status = self.statusBar()
        status.showMessage("Ready", 5000)

    def validateHubble(self):
        '''Update Age in function of the cosmology'''
        
        try: 
            if self.hubbleEdit.text().isEmpty() or self.hubbleEdit.text() == ' ':
                raise ValueError, 'Please enter a Hubble parameter'                
            self.H0 = float(self.hubbleEdit.text())
            if self.H0 <= 0:
                raise ValueError, 'Hubble constant <= 0 not allowed'
            flrw.set_hubble(self.H0)
            return True
        except ValueError, e:
            QMessageBox.warning(self,'Hubble constant error', unicode(e))
            self.hubbleEdit.setText('')
            self.hubbleEdit.selectAll()
            self.hubbleEdit.setFocus()
            return False
        
    def returnHubble(self):
        self.validateHubble()
        if self.hubbleEdit.text().isEmpty() or self.hubbleEdit.text() == ' ':
            self.hubbleEdit.setFocus()
        else:  
            self.matterEdit.setFocus()
    
    def validateMatter(self):
        '''Update Matter density in function of the cosmology'''
        try: 
            if self.matterEdit.text().isEmpty() or self.matterEdit.text() == ' ':
                raise ValueError, 'Please enter a matter density value'                
            self.OmegaM = float(self.matterEdit.text())
            if self.OmegaM < 0:
                raise ValueError, 'Matter density < 0 not allowed'
            flrw.set_matter(self.OmegaM)
            return True        
        except ValueError, e:
            QMessageBox.warning(self,'Matter density error', unicode(e))
            self.matterEdit.setText('')
            self.matterEdit.selectAll()
            self.matterEdit.setFocus()
            return False

    def returnMatter(self):
        self.validateMatter()
        if self.matterEdit.text().isEmpty() or self.matterEdit.text() == ' ':
            self.matterEdit.setFocus()
        else:  
            self.vacuumEdit.setFocus()
                
    def validateVacuum(self):
        '''Update Vacuum density in function of the cosmology'''
        try:
            if self.vacuumEdit.text().isEmpty() or self.vacuumEdit.text() == ' ':
                print self.vacuumEdit.text().isEmpty(), 
                raise ValueError, 'Please enter a vacuum density value'                            
            self.OmegaL = float(self.vacuumEdit.text())
            if self.OmegaL < 0:
                raise ValueError, 'The Universe recolapse'
            flrw.set_vacuum(self.OmegaL)
            return True
        except ValueError, e:
            QMessageBox.warning(self,'Vacuum density error: ', unicode(e))
            self.vacuumEdit.selectAll()
            self.vacuumEdit.setFocus()
            return False

    def returnVacuum(self):
        self.validateVacuum()
        if self.vacuumEdit.text().isEmpty():
            self.vacuumEdit.setFocus()
        else:  
            self.redshiftEdit.setFocus()

    def validateRedshift(self):
        '''Update redshift in function of the cosmology'''
        try:
            if self.redshiftEdit.text().isEmpty():
                raise ValueError, 'Please enter a redshift'
            self.redshift = float(self.redshiftEdit.text())
            if self.redshift < 0:
                raise ValueError, 'The redshift must be positif'
            return True
        except ValueError, e:
            QMessageBox.warning(self,'Redshift error', unicode(e))
            self.redshiftEdit.selectAll()
            self.redshiftEdit.setFocus()
            return False
        
    def returnRedshift(self):
        self.validateRedshift()
        if self.redshiftEdit.text().isEmpty():
            self.redshiftEdit.setFocus()
        else:  
            self.ageCheckBox.setFocus()
            
    def updateAge(self):
        '''Update Age when the cosmology is changed'''
        #self.agelabelres.setText('{0:.2f} Gyr'.format(flrw.age()))
        self.agez0resultLabel.setText('%.2f Gyr' % flrw.age())

    def checkall(self):
        '''Check all the checkbox'''
        for key in self.checkbox.keys():
            key.setChecked(True)

    def uncheckall(self):
        '''Check all the checkbox'''
        for key in self.checkbox.keys():
            key.setChecked(False)

    def apply(self):
        '''Launch the calcul and verification'''
        #TODO: A finir changer la ou le resultat s'affiche! Mettre sous forme de table

        if not self.validateHubble():
            return        
        if not self.validateMatter():
            return
        if not self.validateVacuum():
            return
        if not self.validateRedshift():
            return
        self.updateAge()
        
        for key in self.metrics_method: #self.checkbox.keys():
            if key.isChecked():
                #print self.checkbox[key][0], '=', self.checkbox[key][1](self.redshift), self.checkbox[key][2]
                
                if self.checkbox[key][1] == flrw.vol:
                    #self.resultlabel.setText('{0:s} = {1:.6f} {2:s}'.format(self.checkbox[key][0], self.checkbox[key][1](self.redshift)*1e-9,self.checkbox[key][2]))
                    #print self.checkbox[key][0], '=','%.6f  %s' % (self.checkbox[key][1](self.redshift)*1e-9,self.checkbox[key][2])
                    self.textEdit.insertHtml('%s = %.6f  %s<br>' % (self.checkbox[key][0], self.checkbox[key][1](self.redshift)*1e-9,self.checkbox[key][2]))
                else:
                    #self.resultlabel.setText('{0:s} = {1:.6f} {2:s}'.format(self.checkbox[key][0], self.checkbox[key][1](self.redshift),self.checkbox[key][2]))
                    #print self.checkbox[key][0], '=','%.6f  %s' % (self.checkbox[key][1](self.redshift),self.checkbox[key][2])
                    self.textEdit.insertHtml('%s = %.6f  %s<br>' % (self.checkbox[key][0], self.checkbox[key][1](self.redshift),self.checkbox[key][2]))

        #QtPlaintext
        #self.textEdit.insertPlainText(QString('toto\n'))
        #self.textEdit.insertHtml(QString('toto'))
                      
    def resetForm(self):
        '''Reset the parameters at their default value '''
        
        self.H0 = float(self.__H0)
        flrw.set_hubble(self.H0)
        #self.hubbleEdit.setText('{0:.2f}'.format(self._H0))
        self.hubbleEdit.setText('%.2f ' % (self.__H0))
                
        self.OmegaM = float(self.__OmegaM)
        flrw.set_matter(self.OmegaM)
        #self.matterEdit.setText('{0:.2f}'.format(self._OmegaM))
        self.matterEdit.setText('%.2f' % (self.__OmegaM))
        
        self.OmegaL = float(self.__OmegaL)
        flrw.set_vacuum(self.OmegaL)
        #self.vacuumEdit.setText('{0:.2f}'.format(self._OmegaL))
        self.vacuumEdit.setText('%.2f' % (self.__OmegaL))
 
        self.updateAge()
        self.textEdit.clear()
        self.redshiftEdit.setText('')
        self.redshift = None        

app = QApplication(sys.argv)
pyfunl = PyFunl()
pyfunl.show()
app.exec_()
