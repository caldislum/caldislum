caldislum is is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Caldislum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with Caldislum.  If not, see http://www.gnu.org/licenses/ .

This software is calculating some cosmological parameters as luminosity
distance, age of the universe, comoving volume, lookback time and can 
also do some unit transformation as arcsecond to parsec and the opposite...

It is based on David Hogg's paper:
    
http://xxx.unizar.es/abs/astro-ph/9905116

Most of the result have been successufully compared with with Ned Wright's
cosmology calculator:
    
http://www.astro.ucla.edu/~wright/CosmoCalc.html
