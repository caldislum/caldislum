.. caldislum documentation master file, created by
   sphinx-quickstart on Fri Sep 25 16:34:33 2009.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to caldislum's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2

   caldislum.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

