import os
from setuptools import setup, find_packages
version = '0.1.0'
README = os.path.join(os.path.dirname(__file__), 'README.txt')
long_description = open(README).read() + '\n\n'
setup(name='caldislum',
      version=version,
      description=("A package that provide a GUI to calcul cosmological distances and ages.",),
      long_description=long_description,
      classifiers=[
        "Programming Language :: Python",
        ],
      keywords='astronomy science',
      url='http://gitorious.org/caldislum',
      author='Nicolas Gruel',
      author_email='nicolas.gruel@gmail.com',
      license='GPL',
      packages=find_packages(exclude='tests'),

      package_data = {'': ['*.txt', '*.rst']},

      install_requires=['docutils>=0.5','distribute'] #,'numpydoc>0.3.1'],

      # Faire ca proprement pour la CLI et GUI
      #entry_points = {
      #  'gui_scripts': [
      #      'caldislum = caldislum:main',
      #  ]
    }

     )
